#pragma once
#include "loader_class.h"

using namespace Eigen;

class Function {
	public: 
		int n; //dimensions also number of features
		int m; //number of data points
		float min_value;
		VectorXf minimum;
		MatrixXf A;
		VectorXf b;

		MatrixXf X;
		VectorXf y;

		MatrixXf C;
		VectorXf G;

		float Lipschitz_constant;
		VectorXf Lipschitz_constants_coord;

		Function();
		~Function();
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
		virtual float evaluate( const VectorXf &,const VectorXf &);
 		virtual VectorXf evaluate_derivative( const VectorXf &,const VectorXf &);
 		virtual void Pass_Data_HDF5(Loader*,std::string,std::string,int,int);
 		virtual void Calculate_Residuals(const VectorXf &);
 		virtual VectorXf Calculate_Residuals_for_parallel(const VectorXf &);
 		virtual VectorXf Update_Residuals_for_parallel(const VectorXf & ,const VectorXf & );
 		virtual VectorXf evaluate_coord_derivative_for_parallel(const VectorXf &,int);
 		virtual VectorXf evaluate_coord_derivative_parallel(const VectorXf &,std::string , int );
 		virtual VectorXf evaluate_coord_derivative(const VectorXf &,std::string , int );
        virtual void Calculate_Lipchitz_constant();
        virtual void Calculate_Lipchitz_constant_coord();
        virtual float Get_Lipchitz_constants_coord(int coord);
        virtual float Get_Lipchitz_constant();
        virtual void Set_Residuals_parallel(const VectorXf &);
        virtual void Calculate_Residuals_parallel(const VectorXf &, int);
        virtual float Calculate_Partial_Separability();

};

 class Logistic_Regression : public Function {
    public: 
    	float min_value;
    	VectorXf minimum;

    	MatrixXf X;
		VectorXf y;

		MatrixXf C;
		VectorXf G;
		float previous_gradient;
		int previous_coord;

		//Basics
		Logistic_Regression();
		~Logistic_Regression();
		void Calculate_Residuals(const VectorXf &);
		VectorXf Calculate_Residuals_for_parallel(const VectorXf &);
		VectorXf Update_Residuals_for_parallel(const VectorXf & ,const VectorXf & );

		//Numerics
		float evaluate( const VectorXf &,const VectorXf &);
		VectorXf evaluate_derivative( const VectorXf &,const VectorXf &);
		VectorXf evaluate_coord_derivative(float,const VectorXf &,int);
		VectorXf evaluate_coord_derivative_for_parallel(const VectorXf &,int);
		//VectorXf evaluate_single_derivative( const VectorXf &,const VectorXf &,int);
		
		//Data Filling
		void Pass_Data(const MatrixXf &, const VectorXf &); //Feature Matrix, Label Matrix
		void Pass_Data_HDF5(Loader*,std::string,std::string,int,int); //Feature Matrix, Label Matrix

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
 };

  class Least_Squares_Regression : public Function {
	public: 
		//main variables
		bool full;
		MatrixXf A;
		VectorXf b;
		//residual variables
		MatrixXf ATA;
		VectorXf residual;
		VectorXf residual_old;
		VectorXf ATb;
		float previous_gradient;
		int previous_coord;
		VectorXf previous;
		float beta;
		float omega;
		float Lipschitz_constant;
		VectorXf Lipschitz_constants_coord;
		//Basics
		Least_Squares_Regression();
		~Least_Squares_Regression();
		void Calculate_Residuals(const VectorXf &);
		//void Calculate_Residuals_for_parallel(const VectorXf &);
		//Numerics
		float evaluate( const VectorXf &,const VectorXf &);
		VectorXf evaluate_derivative( const VectorXf &,const VectorXf &);
		VectorXf evaluate_coord_derivative(const VectorXf &,std::string, int);
		VectorXf evaluate_coord_derivative_parallel(const VectorXf &,std::string, int);

		float evaluate_single( const VectorXf &,const VectorXf &,int);
		VectorXf evaluate_single_derivative( const VectorXf &,const VectorXf &,int);
		
		//Data Filling
		void Pass_Data(const MatrixXf &, const VectorXf &); //Feature Matrix, Label Matrix
		void Pass_Data_HDF5(Loader*,std::string,std::string,int,int); //Feature Matrix, Label Matrix
        void Calculate_Lipchitz_constant();
        void Calculate_Lipchitz_constant_coord();
        float Get_Lipchitz_constants_coord(int coord);
        float Get_Lipchitz_constant();
        float Calculate_Partial_Separability();
        void Set_Residuals_parallel(const VectorXf &) ;
        void Calculate_Residuals_parallel(const VectorXf &, int);
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
 };
