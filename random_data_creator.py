import random
import time
import h5py
import numpy as np
import math

def Create_Least_Squares_Data(name,n,m):
	f=h5py.File(name, "w", libver='latest')
	data=f.create_dataset("data", (n,m), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	lab=f.create_dataset("labels", (n,1), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	A=np.random.rand(n,1);
	lab[:,:]=A
	B=np.random.rand(n,m);
	data[:,:]=B
	f.close()
def Create_Logistic_Reg_Data(name,n,m):
	f=h5py.File(name, "w", libver='latest')
	data=f.create_dataset("data", (n,m), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	lab=f.create_dataset("labels", (n,1), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	A=(-1)*np.ones((n,1),dtype=int)
	for i in np.arange(0,n,1):
		A[i,0]=math.pow(A[i,0],random.randint(1,2));
	lab[:,:]=A
	B=np.random.rand(n,m);
	data[:,:]=B
	f.close()

Create_Logistic_Reg_Data("../biglearning_data/Log_Data_n_10_m_10.h5",10,10)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_10_m_10.h5",10,10)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_100_m_100.h5",100,100)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_1000_m_1000.h5",1000,1000)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_10000_m_10000.h5",10000,10000)
"""
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_5000_m_5000.h5",5000,5000)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_8000_m_8000.h5",8000,8000)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_10000_m_10000.h5",10000,10000)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_20000_m_20000.h5",20000,20000)

"""
