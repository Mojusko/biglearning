#pragma once
#include "loader_class.h"
#include "function_class.h"

//---------------------------------------------------------------------------------
//-------------------General Stopping Criteria Stuff-------------------------------
//---------------------------------------------------------------------------------

// We have 5 possible models, depending on the constructur we can intiialize different 
//objects 

// "Iterations", N
// "Error", Epsilon
// "Change", Delta
// "Time", Time
// "Combination", Epsilon, Delta, Time


class Stop_Criteria {
	public:
	Stop_Criteria();
	~Stop_Criteria();
	virtual bool Test(int,Function* ,const VectorXf  &,const VectorXf  & ,double);
};

class Iter_Stop  : public Stop_Criteria {
    public: 
    	int N;
    	Iter_Stop(int);
    	~Iter_Stop();
    	bool Test(int,Function* ,const VectorXf  &,const VectorXf  & ,double);
};

class Error_Stop : public Stop_Criteria{
    public: 
    	float Error;
    	float Min;
    	Error_Stop(float,float);
    	~Error_Stop();
    	bool Test(int,Function* ,const VectorXf  &,const VectorXf  & ,double);
};
  
  /*
class Change_Stop: public Stop_Criteria{
    public: 
    	float Change;
    	Change_Stop(float);
    	~Change_Stop();
    	Test(int,float,float,double);
    	};
*/
class Time_Stop  : public Stop_Criteria{
    public: 
    	double Time;
        double Start;
        int c;
    	Time_Stop(int, float, float);
    	~Time_Stop();
        bool Test(int,Function* ,const VectorXf  &,const VectorXf  & ,double);  
};

/*
class Combined_Stop  : public Stop_Criteria{
    public: 
    	};
*/