#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
	
#include "function_class.h"
#include "loader_class.h"
using namespace Eigen;
using namespace std;
//---------------------------
const float epsilon=0.00000001;


//---------------------------------------------------------------------------------
// -------------------------- GENERAL PARENT CLASS---------------------------------
//---------------------------------------------------------------------------------

Function::Function(){}
Function::~Function(){}
float Function::evaluate(const VectorXf & x, const VectorXf & w){}
VectorXf Function::evaluate_derivative(const VectorXf & x, const VectorXf & w){}
VectorXf Function::evaluate_coord_derivative(const VectorXf &x,std::string step_c, int j){}
VectorXf Function::evaluate_coord_derivative_for_parallel( const VectorXf & w,int i){}

void Function::Pass_Data_HDF5(Loader*,std::string,std::string,int,int) {}
void Function::Calculate_Residuals(const VectorXf & x0){}
void Function::Calculate_Residuals_parallel(const VectorXf & x0,int ){}
VectorXf Function::Calculate_Residuals_for_parallel(const VectorXf & x0){}
VectorXf Function::Update_Residuals_for_parallel(const VectorXf & G,const VectorXf & delta){}
void Function::Calculate_Lipchitz_constant(){}
void Function::Calculate_Lipchitz_constant_coord(){}
float Function::Get_Lipchitz_constant(){}
float Function::Get_Lipchitz_constants_coord(int coord){}
void Function::Set_Residuals_parallel(const VectorXf & update){}
float Function::Calculate_Partial_Separability(){}
VectorXf Function::evaluate_coord_derivative_parallel(const VectorXf &x,std::string step_c, int j){}
//---------------------------------------------------------------------------------
//----------------------------LOGISTIC REGRESSION----------------------------------
//---------------------------------------------------------------------------------
Logistic_Regression::Logistic_Regression() 
{
	std::cout << "\033[1;32m  OK: Logistic Regression Function has been created.\033[0m" << std::endl;
}
Logistic_Regression::~Logistic_Regression(){}

void Logistic_Regression::Calculate_Residuals(const VectorXf & x0){
	cout << "\033[1;34mNOTE: Calculating Residuals ....\033[0m" << endl;
	
	this->C=MatrixXf::Zero(this->m,this->n);

	for (int i=0;i<this->m;i++){
		C.row(i)=y(i)*X.row(i);
	}	
	// Fill in Residual
	G=VectorXf::Zero(this->m);
	for (int i=0;i<this->m;i++) {
		 G(i)=exp(C.row(i)*x0);
	}

	VectorXf w; //weights
	w=VectorXf::Zero(this->n);
	previous_gradient=this->evaluate_derivative(x0,w)(0);
	this->previous_coord=0;

	cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
}

VectorXf Logistic_Regression::Calculate_Residuals_for_parallel(const VectorXf & x0){
	cout << "\033[1;34mNOTE: Calculating Residuals ....\033[0m" << endl;
		
	this->C=MatrixXf::Zero(this->m,this->n);

	for (int i=0;i<this->m;i++){
		this->C.row(i)=this->y(i)*this->X.row(i);
	}	
	VectorXf N;
	N=VectorXf::Zero(this->m);
	for (int i=0;i<this->m;i++) {
		 N(i)=exp(this->C.row(i)*x0);
	}
	cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
	return N;
}

VectorXf Logistic_Regression::Update_Residuals_for_parallel(const VectorXf & G,const VectorXf & Delta)
{
	//cout << "\033[1;32mNOTE: Updating Residuals.\033[0m" << std::endl;
	VectorXf N;
	N=VectorXf::Zero(this->m);
	for (int i=0;i<this->m;i++){
			N(i)=G(i)*exp(-this->y(i)*this->X.row(i)*Delta);
	}
	return N;
}



float Logistic_Regression::evaluate(const VectorXf & x, const VectorXf & w)
{
	float log_likehood=0.0;
	for (int i=0;i<this->m;i++){
		log_likehood+=log(1+exp(-this->y(i)*this->X.row(i)*x));
	}
	return log_likehood/(float)(this->m);
}


VectorXf Logistic_Regression::evaluate_derivative( const VectorXf &x,const VectorXf &w)
{
	VectorXf gradient;
	gradient=VectorXf::Zero(this->n);
	for (int i=0;i<this->m;i++){
		gradient+=this->C.row(i)/(1+exp(this->C.row(i)*x));
	
	}
	return (-1)*gradient/(float)(this->m);

}
VectorXf Logistic_Regression::evaluate_coord_derivative( float step_size,const VectorXf &x,int j)
{

	VectorXf e_i;
	VectorXf e_j;
	e_j=VectorXf::Zero(this->n);
	e_j(j)=1;
	e_i=VectorXf::Zero(this->n);
	int i;
	i=this->previous_coord;
	e_i(i)=1;

	float update;
	float gradient;
	gradient=0;
	for (int k=0;k<this->m;k++){
		update=exp((-1)*this->y(k)*this->previous_gradient*this->X.row(k)(i));
		this->G(k)=this->G(k)*update;
		gradient+=step_size*this->C.row(k)(j)/(1+this->G(k));
	}
	this->previous_gradient=(-1)*gradient/(float)(this->m);
	this->previous_coord=j;
	return (-1)*gradient/(float)(this->m)*e_j;

}

VectorXf Logistic_Regression::evaluate_coord_derivative_for_parallel(const VectorXf &N,int j)
{
	VectorXf e_j;	
	e_j=VectorXf::Zero(this->n);
	e_j(j)=1;

	float gradient;
	for (int k=0;k<this->m;k++){
		gradient+=this->C.row(k)(j)/(1+N(k));
	}
	return (-1)*gradient/(float)(this->m)*e_j;
}


void Logistic_Regression::Pass_Data_HDF5(Loader * L,std::string dts1,std::string dts2,int n,int m)
{
	this->X=L->Load_Data_Set_Full(dts1,n,m);
	this->y=L->Load_Data_Set_Full(dts2,n,1);
	this->n=this->X.cols();
	this->m=this->X.rows();
	std::cout << "\033[1;32m  OK: Data has been successfully assigned to Logistic Regressor.\033[0m" << std::endl;
}



//---------------------------------------------------------------------------------
//----------------------------LEAST SQUARES REGRESSION-----------------------------
//---------------------------------------------------------------------------------
Least_Squares_Regression::Least_Squares_Regression() 
{
	std::cout << "\033[1;32m  OK: Least Squares Regression Function has been created.\033[0m" << std::endl;
}
Least_Squares_Regression::~Least_Squares_Regression(){}

void Least_Squares_Regression::Calculate_Residuals(const VectorXf & x0){
	cout << "\033[1;34mNOTE: Calculating Residuals ....\033[0m" << endl;
	this->ATA=(this->A.transpose())*(this->A);
	this->ATb=(this->A.transpose())*(this->b);
	VectorXf w; //weights
	w=VectorXf::Zero(this->n);
	this->residual=this->ATA*x0;
	this->residual_old=this->residual;
	this->previous_gradient=w(0);
	this->previous_coord=0;
	this->Calculate_Partial_Separability();
	cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
}


void Least_Squares_Regression::Calculate_Residuals_parallel(const VectorXf & x0,int c){
	cout << "\033[1;34mNOTE: Calculating Residuals ....\033[0m" << endl;
	this->ATA=(this->A.transpose())*(this->A);
	this->ATb=(this->A.transpose())*(this->b);
	VectorXf w; //weights
	w=VectorXf::Zero(this->n);
	this->residual=ATA*x0;
	this->residual_old=ATA*x0;
	w=(this->ATA*x0);
	this->previous=VectorXf::Zero(this->n);
	this->Calculate_Partial_Separability();
	this->beta=1+(this->omega-1)*(c-1)/(this->n-1);
	cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
}


//-------FULL GRADIENT
float Least_Squares_Regression::evaluate(const VectorXf & x, const VectorXf & w)
{
	VectorXf M;
	M=(this->A*x-b);
	return (1/2.0)*M.dot(M);
}
VectorXf Least_Squares_Regression::evaluate_derivative( const VectorXf &x,const VectorXf &w)
{
	MatrixXf Der;
	Der=this->ATA*x-this->ATb;
	return Der;
}
//------COORDINATE GRADIENT
VectorXf Least_Squares_Regression::evaluate_coord_derivative( const VectorXf &x,std::string step_criteria, int j )
{
		VectorXf update;
		VectorXf e_i;
		VectorXf e_j;
		e_j=VectorXf::Zero(this->n);
		e_j(j)=1;
		e_i=VectorXf::Zero(this->n);
		int i;
		i=this->previous_coord;
		e_i(i)=1;
		float nabla_j_f;
		//-------------------------------------------------
		//-------------------------------------------------
		
		nabla_j_f=this->residual_old(j)-(1.0/(this->Lipschitz_constants_coord(i)))*(this->ATA.col(i)(j))*this->previous_gradient-this->ATb(j);
		
		update=-(1.0/(this->Lipschitz_constants_coord(j)))*nabla_j_f*e_j;
		this->previous_gradient=nabla_j_f;
		this->previous_coord=j;
		this->residual_old=this->residual;
		this->residual=this->residual+this->ATA*update;
		
		return update;
}


VectorXf Least_Squares_Regression::evaluate_coord_derivative_parallel( const VectorXf &x,std::string step_criteria, int j )
{

		VectorXf update;
		VectorXf e_j;
		e_j=VectorXf::Zero(this->n);
		e_j(j)=1;
		float nabla_j_f;
		//-------------------------------------------------
		//-------------------------------------------------
		VectorXf c;
		c=(this->ATA*this->previous);
		nabla_j_f=this->residual_old(j)-c(j)-this->ATb(j);
		update=-(1.0/(this->Lipschitz_constants_coord(j)))*(nabla_j_f*e_j);
		

		return update;
}

void Least_Squares_Regression::Set_Residuals_parallel(const VectorXf & update) {
	this->residual_old=this->residual;
	this->residual=this->residual+this->ATA*update;
	this->previous=-update;
}

//------SUM GRADIENT
float Least_Squares_Regression::evaluate_single( const VectorXf & x ,const VectorXf & w,int i){}
VectorXf Least_Squares_Regression::evaluate_single_derivative( const VectorXf & x ,const VectorXf & w,int i){}
//-------------






//Data Filling
void Least_Squares_Regression::Pass_Data(const MatrixXf & A_data,const VectorXf & b_data)
{
	this->A=A_data;
	this->b=b_data;
	std::cout << "\033[1;32mNOTE: Data has been successfully assigned to Least Square Regressor.\033[0m" << std::endl;
}

void Least_Squares_Regression::Pass_Data_HDF5(Loader * L,std::string dts1,std::string dts2,int n,int m)
{
    this->A=L->Load_Data_Set_Full(dts1,n,m);
    this->b=L->Load_Data_Set_Full(dts2,n,1);
    this->n=this->A.cols();
    this->m=this->A.rows();
    std::cout << "\033[1;32m  OK: Data has been successfully assigned to Least Square Regressor.\033[0m" << std::endl;
}


// Lipchitz constants settings
void Least_Squares_Regression::Calculate_Lipchitz_constant()
{
// Two possible methods to calculate largest eigenvalue of the matrix. Numerical approach would be Power Method. The second would be conventional solving of linear system. Following Function implements linear system approach.
	std::cout << "\033[1;34mNOTE: Calculating Lipchitz Constant...\033[0m" << std::endl;
    SelfAdjointEigenSolver<MatrixXf> eigensolver(this->A.transpose()*this->A);
    VectorXf v;
    v=eigensolver.eigenvalues();
    float maxOfv;
    maxOfv = v.maxCoeff();
    std::cout << maxOfv << endl;
    this->Lipschitz_constant=maxOfv;
    std::cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
}

void Least_Squares_Regression::Calculate_Lipchitz_constant_coord()
{
	this->Lipschitz_constants_coord=VectorXf::Zero(this->n);
	std::cout << "\033[1;34m  OK: Calculating Lipchitz Constants in Directions...\033[0m" << std::endl;
	for (int i=0;i<this->n;i++){
		float L;
		L=((this->A.col(i)).norm())*((this->A.col(i)).norm());
		this->Lipschitz_constants_coord(i)=L;
		//std::cout << L << endl;
	}
	std::cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
}


float Least_Squares_Regression::Calculate_Partial_Separability(){
	// Finding max (for all collums (non-zero value))
	//-----------------------------------------------
	int No;
	int NoMax=0;
	std::cout << "\033[1;34m  OK: Calculating Degree of Parial Separability...\033[0m" << std::endl;
	for (int i=0;i<this->m;i++) {
		No=0;
		for (int j=0;j<this->n;j++){
			if (abs(this->A.row(i)(j))>epsilon) {
				No++;
			}
		}
		if (NoMax<No){
			NoMax=No;
		}
	}

	this->omega=NoMax;
	std::cout << "Omega: "<< NoMax << " N: " << this->n 	<< endl;
	std::cout << "\033[1;32m  OK: Done.\033[0m" << std::endl;
	return NoMax;
}


float  Least_Squares_Regression::Get_Lipchitz_constant(){
	return this->Lipschitz_constant;
}
float  Least_Squares_Regression::Get_Lipchitz_constants_coord(int coord){
	return this->Lipschitz_constants_coord(coord);
}



