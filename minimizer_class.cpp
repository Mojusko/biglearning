#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include "omp.h"
#include <vector>
#include <algorithm>    // std::random_shuffle

#include "function_class.h"
#include "minimizer_class.h"
#include "loader_class.h"
#include "stopping_criteria_class.h"

using namespace Eigen;
using namespace std;

//---------------------------------------------------------------------------------
//-------------------General Parent minimizer Class-----------------
//---------------------------------------------------------------------------------

Minimizer::Minimizer() {}
Minimizer::~Minimizer(){}

VectorXf Minimizer::Get_Min()
{
	return this->func->minimum;
}

//---------------------------------------------------------------------------------
//-------------------------------FULL GRADIENT DESCEND ----------------------
//---------------------------------------------------------------------------------
Full_Gradient::Full_Gradient(Function* F)
{
	this->func=F;
	cout << "\033[1;32m  OK: Full Gradient Minimizer has been created\033[0m" << endl;
}
Full_Gradient::~Full_Gradient(){}

float Full_Gradient::Optimize(std::string step_criteria,Stop_Criteria* stop_criteria,std::string start_criteria)
{

	//Verbose Output
	cout << "\033[1;34mNOTE: Minimizing the function with Full Gradient....\033[0m" << endl;
	
	// Lipchitz constant 
	float step_size;
	if (step_criteria=="Lipschitz"){
		this->func->Calculate_Lipchitz_constant();
		step_size=1.0/(this->func->Get_Lipchitz_constant());
		cout << step_size << endl;
	}

	if (start_criteria=="Random"){
		cout << "\033[1;34mNOTE: Creating Random Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Random(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		this->func->Calculate_Residuals(x0);
		//Full Gradient Method for minimization with fixed stepsize
		
		VectorXf x(this->func->n); //variable
		VectorXf gradient(this->func->n);
		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		while (!stop_criteria->Test(iterate,func,x,w,clock())) 
		{
			gradient=this->func->evaluate_derivative(x,w);
			x=x-step_size*(gradient);
			//cout << "Iter: " << iterate << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}

		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;
		}
	else if (start_criteria=="Origin"){
	cout << "\033[1;34mNOTE: Creating Origin Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Zero(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		this->func->Calculate_Residuals(x0);
		//Full Gradient Method for minimization with fixed stepsize
		
		VectorXf x(this->func->n); //variable
		VectorXf gradient(this->func->n);
		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		while (!stop_criteria->Test(iterate,func,x,w,clock())) 
		{
			gradient=this->func->evaluate_derivative(x,w);
			x=x-step_size*(gradient);
			//cout << "Iter: " << iterate << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}

		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;

	}
	else
	{
		cout << "\033[1;31m ERR: Unknown Command.\033[0m" << endl;
	}
	
}
//---------------------------------------------------------------------------------
//------------------------------- COORDINATE DESCEND ----------------------
//---------------------------------------------------------------------------------
Random_Coordinate_Descend::Random_Coordinate_Descend(Function* F)
{
	this->func=F;
	cout << "\033[1;32m  OK: Coordinate Descend Minimizer has been created\033[0m" << endl;
}
Random_Coordinate_Descend::~Random_Coordinate_Descend(){}

float Random_Coordinate_Descend::Optimize(std::string step_criteria,Stop_Criteria* stop_criteria,std::string start_criteria)
{

	//Verbose Output
	cout << "\033[1;34mNOTE: Minimizing the function with Coordinate Descend ....\033[0m" << endl;


	if (step_criteria=="Lipschitz"){
		this->func->Calculate_Lipchitz_constant_coord();
	}

	if (start_criteria=="Random"){
		// Random Start
		cout << "\033[1;34mNOTE: Creating Origin Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Random(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		// Initialization of variables
		this->func->Calculate_Residuals(x0);
		VectorXf x(this->func->n); //variable
		VectorXf update(this->func->n);

		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		srand(time(NULL));
		int coord;
		
		// Loop until happy
		while (!stop_criteria->Test(iterate,func,x,w,clock())) {
			coord=rand() % (this->func->n);
			update=this->func->evaluate_coord_derivative(x,step_criteria,coord);
			x=x+update;
			//cout << "Iter CD: " << iterate << " taking coord: " << coord << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}

		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;
		}
	else if (start_criteria=="Origin"){
		// Origin Start
		cout << "\033[1;34mNOTE: Creating Random Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Zero(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		// Initialization of variables
		this->func->Calculate_Residuals(x0);
		VectorXf x(this->func->n); //variable
		VectorXf update(this->func->n);

		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		srand(time(NULL));
		int coord;
		
		// Loop until happy
		while (!stop_criteria->Test(iterate,func,x,w,clock())) {
			coord=rand() % (this->func->n);
			update=this->func->evaluate_coord_derivative(x,step_criteria,coord);
			x=x+update;
			//cout << "Iter CD: " << iterate << " taking coord: " << coord << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}

		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;

		}
	else
	{
		cout << "\033[1;31m ERR: Unknown Command.\033[0m" << endl;
	}
	
}
//---------------------------------------------------------------------------------
//-------------------------------PARALELL COORDINATE DESCEND ----------------------
//---------------------------------------------------------------------------------

Parallel_Random_Coordinate_Descend::Parallel_Random_Coordinate_Descend(Function* F)
{
	this->func=F;
	cout << "\033[1;32m  OK: Parallel Coordinate Descend Minimizer has been created\033[0m" << endl;
}
Parallel_Random_Coordinate_Descend::~Parallel_Random_Coordinate_Descend(){}

float Parallel_Random_Coordinate_Descend::Optimize(int c,std::string step_criteria,Stop_Criteria* stop_criteria,std::string start_criteria)
{
	
	//Verbose Output
	cout << "\033[1;34mNOTE: Minimizing the function with Coordinate Descend ....\033[0m" << endl;


	if (step_criteria=="Lipschitz"){
		this->func->Calculate_Lipchitz_constant_coord();
	}
	float omega=this->func->Calculate_Partial_Separability();

	if (start_criteria=="Random"){
		// Random Start
		cout << "\033[1;34mNOTE: Creating Random Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Random(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		// Initialization of variables
		this->func->Calculate_Residuals_parallel(x0,c);
		VectorXf x(this->func->n); //variable
		VectorXf update(this->func->n);
		VectorXf update2(this->func->n);


		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		srand(time(NULL));
		MatrixXf updates;
		MatrixXf updates2;

		updates=MatrixXf::Zero(this->func->n,c);
		updates2=MatrixXf::Zero(this->func->n,c);
		update=VectorXf::Zero(this->func->n);

		// Loop until happy
		float beta=1+((omega-1.0)*(c-1.0))/(this->func->n-1.0);
		beta=c;
		// Create a sampling array
		std::srand ( unsigned ( time(NULL) ) );
	  	std::vector<int> myvector;

		// set some values:
		for (int i=0; i<this->func->n; i++) myvector.push_back(i); // 1 2 3 4 5 6 7 8 9
		// using myrandom:
		//-----------------------
		while (!stop_criteria->Test(iterate,func,x,w,clock()))  {
			update=VectorXf::Zero(this->func->n);
			//update2=VectorXf::Zero(this->func->n);
			std::random_shuffle ( myvector.begin(), myvector.end());
			//--------------
			#pragma omp parallel for
			for (int i=0;i<c;i++)
			{

				int coord;
				coord=myvector[i];
			//	cout << "Process i: "<< i << " taking coord: " << coord << endl; 
				update(coord)=this->func->evaluate_coord_derivative_parallel(x,step_criteria,coord)(coord);
			//	cout << i << " INLOOP UPDATE: " <<updates.col(i).transpose() << endl;
			//	cout << "---------" << endl;
			/*
				VectorXf e_j;
				e_j=VectorXf::Zero(this->func->n);
				e_j(coord)=1;
				updates2.col(i)=-(1.0/(this->func->Get_Lipchitz_constants_coord(coord)))*this->func->evaluate_derivative(x,x)(coord)*(e_j);
				cout << i << " INLOOP UPDATE2: " <<updates2.col(i).transpose() << endl;
			*/
			}

			
			/*for (int i=0;i<c;i++)
			{
				update=update+updates.col(i);
			}*/
			

			/*
			for (int i=0;i<c;i++)
			{
				update2=update2+updates2.col(i);
			}
			*/
			
			//cout << update.transpose() << endl;
			//cout << update2.transpose() << endl;			
			
			// Now I can update residuals
			this->func->Set_Residuals_parallel((1.0/beta)*update);
			//
			x=x+(1.0/beta)*update;
			//cout << "Iter PCDM: " << iterate << " taking coord: " << "Multiple" << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}


		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;
		}
		else if (start_criteria=="Origin"){
			// Random Start
		cout << "\033[1;34mNOTE: Creating Origin Start Point ....\033[0m" << endl;
		VectorXf x0; //start
		x0=VectorXf::Zero(this->func->n);
		//cout << "Random Vector: " << x0.transpose() << endl;
		// Initialization of variables
		this->func->Calculate_Residuals_parallel(x0,c);
		VectorXf x(this->func->n); //variable
		VectorXf update(this->func->n);
		VectorXf update2(this->func->n);


		x=x0;  // Start
		VectorXf w; //weights
		w=VectorXf::Ones(this->func->n);
		float iterate;
		iterate=0;
		srand(time(NULL));
		MatrixXf updates;
		MatrixXf updates2;

		updates=MatrixXf::Zero(this->func->n,c);
		updates2=MatrixXf::Zero(this->func->n,c);
		update=VectorXf::Zero(this->func->n);

		// Loop until happy
		float beta=1+((omega-1.0)*(c-1.0))/(this->func->n-1.0);
		beta=c;
		// Create a sampling array
		std::srand ( unsigned ( time(NULL) ) );
	  	std::vector<int> myvector;

		// set some values:
		for (int i=0; i<this->func->n; i++) myvector.push_back(i); // 1 2 3 4 5 6 7 8 9
		// using myrandom:
		//-----------------------
		while (!stop_criteria->Test(iterate,func,x,w,clock()))  {
			update=VectorXf::Zero(this->func->n);
			//update2=VectorXf::Zero(this->func->n);
			std::random_shuffle ( myvector.begin(), myvector.end());
			//--------------
			#pragma omp parallel for
			for (int i=0;i<c;i++)
			{

				int coord;
				coord=myvector[i];
			//	cout << "Process i: "<< i << " taking coord: " << coord << endl; 
				update(coord)=this->func->evaluate_coord_derivative_parallel(x,step_criteria,coord)(coord);
			//	cout << i << " INLOOP UPDATE: " <<updates.col(i).transpose() << endl;
			//	cout << "---------" << endl;
			/*
				VectorXf e_j;
				e_j=VectorXf::Zero(this->func->n);
				e_j(coord)=1;
				updates2.col(i)=-(1.0/(this->func->Get_Lipchitz_constants_coord(coord)))*this->func->evaluate_derivative(x,x)(coord)*(e_j);
				cout << i << " INLOOP UPDATE2: " <<updates2.col(i).transpose() << endl;
			*/
			}

			
			/*for (int i=0;i<c;i++)
			{
				update=update+updates.col(i);
			}*/
			

			/*
			for (int i=0;i<c;i++)
			{
				update2=update2+updates2.col(i);
			}
			*/
			
			//cout << update.transpose() << endl;
			//cout << update2.transpose() << endl;			
			
			// Now I can update residuals
			this->func->Set_Residuals_parallel((1.0/beta)*update);
			//
			x=x+(1.0/beta)*update;
			//cout << "Iter PCDM: " << iterate << " taking coord: " << "Multiple" << " " << func->evaluate(x,w) << endl;
			iterate=iterate+1;
		}


		this->func->minimum=x;
		cout << "DEB Total Iterations: " << iterate << endl;
		cout << "DEB Function Minimum: " << func->evaluate(x,w) << endl;
		cout << "\033[1;32m  OK: Done.\033[0m" << endl;
		}
	else
	{
		cout << "\033[1;31m ERR: Unknown Command.\033[0m" << endl;
	}

}
