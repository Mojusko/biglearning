#pragma once
#include <Eigen/Dense>
#include <Eigen/Eigen>
#include <vigra/hdf5impex.hxx>

using namespace Eigen;
using namespace vigra;

class Loader {
   public: 
	Loader();
	~Loader();
	virtual MatrixXf Load_Data_Set_Full(std::string,int, int){};
 };

 class Loader_HDF5 : public Loader {
   public: 
   	std::string mode;
	std::string name;

	Loader_HDF5();
	~Loader_HDF5();
	void Load(std::string);
	void Set_Access_Mode(std::string);
	MatrixXf Load_Data_Set_Full(std::string,int, int);
	MatrixXf Load_Data_Set_Slice();
	void Close();
};