#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <sstream>
#include "function_class.h"
#include "minimizer_class.h"
#include "loader_class.h"
#include "stopping_criteria_class.h"

using namespace Eigen;
using namespace std;

int main() {

clock_t start;
clock_t ends;

// Load Data
Loader_HDF5 Loader;
Loader.Set_Access_Mode("RAM");

Loader.Load("../LeSqr_Data_n_100_m_100.h5");
// Create Function Model
Logistic_Regression F;
Least_Squares_Regression F2;
// Pass Data to the Function
F2.Pass_Data_HDF5(&Loader,"data","labels",100,100);
// Create Min Model and Minimize
Full_Gradient Min1(&F2);
Random_Coordinate_Descend Min2(&F2);
Parallel_Random_Coordinate_Descend Min3(&F2);
//----------------------------------

// Clock to measure time
start = clock();
double Time=10;
Time_Stop Criteria(1,Time,clock()); //Terminates after Time second, first argument signifies number of cores running
// Minimizes the function, with stepsizes based on Lipchitz constants with Random starting point, Full Gradient method
Min1.Optimize("Lipschitz",&Criteria,"Random");
cout << Min1.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / CLOCKS_PER_SEC << endl;


start = clock();
Error_Stop Criteria2(0,0.1); // First argument corresponds to Func. Min, and second to desired error
// Minimizes the function, with stepsizes based on Lipchitz constants with Random starting point, PCDM with number of cores=2
Min3.Optimize(2,"Lipschitz",&Criteria2,"Random");
cout << Min3.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / CLOCKS_PER_SEC << endl;



return 0;

}
