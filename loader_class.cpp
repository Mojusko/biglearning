#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <vigra/multi_array.hxx>

#include "loader_class.h"

using namespace Eigen;
using namespace std;
using namespace vigra;



//General Parent Loader Class

Loader::Loader() {

}
Loader::~Loader(){}

// ---- HDF5 Files

Loader_HDF5::Loader_HDF5() {
	this->mode="RAM";
}
Loader_HDF5::~Loader_HDF5(){}

void Loader_HDF5::Set_Access_Mode(std::string mode){
	this->mode=mode;
}


void Loader_HDF5::Load(std::string name)
{
	if (this->mode=="RAM"){
			cout << "\033[1;34mNOTE: Setting acess to: " << name <<" ....\033[0m" << endl;
			//HDF5File file2(name,HDF5File::Open);
			this->name=name;
			cout << "\033[1;32m  OK: Done.\033[0m" << endl;
	}
	else{ 
		if (this->mode=="DSK") {
			cout << "\033[1;31mERR: DSK Access is not implemented yet.\033[0m" << endl;}
		}
	}

void Loader_HDF5::Close(){
		//this->file.close();
}


MatrixXf Loader_HDF5::Load_Data_Set_Full(std::string dts,int m, int n)
{
	MatrixXf data(m,n);
	if (this->mode=="RAM"){
			cout << "\033[1;34mNOTE: Loading the data: '" << dts << "' from: " << this->name <<" ....\033[0m" << endl;
			HDF5File file(this->name,HDF5File::Open);
			int width = m;
			int height = n;
			MultiArray<2, double> arr(Shape2(height,width));
			file.read(dts, arr);
			file.close();
			for (int i=0;i<m;i++){
				for (int j=0;j<n;j++){
					float a;
					a=arr(j,i);
					data(i,j)=a;
				}
			}
			//cout << data << endl;
			cout << "\033[1;32m  OK: Done.\033[0m" << endl;
			return data;
	}
	else
	{ 
		if (this->mode=="DSK") {
			cout << "\033[1;31mERR: DSK Mode selected. Cannot Load Full Matrix.\033[0m" << endl;
			return data;
		}
	}

}


MatrixXf Loader_HDF5::Load_Data_Set_Slice()
{

}