#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <sstream>
#include "function_class.h"
#include "minimizer_class.h"
#include "loader_class.h"
#include "stopping_criteria_class.h"

using namespace Eigen;
using namespace std;

int main() {

for (int j=10;j<100000;j=10*j) {

clock_t start;
clock_t ends;

// Load Data
Loader_HDF5 Loader;
Loader.Set_Access_Mode("RAM");

cout <<  "DEB DATASET SIZE:" <<j << endl;
std::string s;  
ostringstream ss;
ss << j;
s=ss.str();


Loader.Load("../../biglearning_data/LeSqr_Data_n_"+s+"_m_"+s+".h5");
// Create Function Model
Logistic_Regression F;
Least_Squares_Regression F2;
// Pass Data to the Function
F2.Pass_Data_HDF5(&Loader,"data","labels",j,j);
// Create Min Model and Minimize
Full_Gradient Min1(&F2);
Random_Coordinate_Descend Min2(&F2);
Parallel_Random_Coordinate_Descend Min3(&F2);
//----------------------------------

// Clock to measure time
start = clock();
double Time=60;
Time_Stop Criteria0(1,Time,clock());

// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=1
Min1.Optimize("Lipschitz",&Criteria0,"Origin");
cout << Min1.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / CLOCKS_PER_SEC << endl;


Time_Stop Criteria01(1,Time,clock());

start = clock();
// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=2
Min2.Optimize("Lipschitz",&Criteria01,"Origin");
cout << Min2.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / CLOCKS_PER_SEC << endl;

int c=1;
Time_Stop Criteria1(c,Time,clock());
start = clock();
// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=2
Min3.Optimize(c,"Lipschitz",&Criteria1,"Origin");
cout << Min3.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / (c*CLOCKS_PER_SEC) << endl;



start = clock();
// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=2
c=2;
Time_Stop Criteria2(c,Time,clock());
Min3.Optimize(c,"Lipschitz",&Criteria2,"Origin");
cout << Min3.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / (c*CLOCKS_PER_SEC) << endl;


start = clock();
// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=2
c=3;
Time_Stop Criteria3(c,Time,clock());
Min3.Optimize(c,"Lipschitz",&Criteria3,"Origin");
cout << Min3.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / (c*CLOCKS_PER_SEC) << endl;


start = clock();
// Minimizes the function, with stepsizes based on Lipchitz constants with Origin starting point, number of cores=2
c=4;
Time_Stop Criteria4(c,Time,clock());
Min3.Optimize(c,"Lipschitz",&Criteria4,"Origin");
cout << Min3.Get_Min().transpose() << endl;

ends = clock();
cout << "DEB Time elapsed: " << (double) (ends - start) / (c*CLOCKS_PER_SEC) << endl;

}

return 0;

}
