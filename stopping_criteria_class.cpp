#include "stopping_criteria_class.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>

// General Class
Stop_Criteria::Stop_Criteria() {}
Stop_Criteria::~Stop_Criteria(){}

bool Stop_Criteria::Test(int a ,Function* func,const VectorXf  & c,const VectorXf  & d,double t){}
// Iter_Class
Iter_Stop::Iter_Stop(int a) 
{
	this->N=a;
}
Iter_Stop::~Iter_Stop(){};

bool Iter_Stop::Test(int a,Function* func,const VectorXf  & c,const VectorXf  & d,double t){
	if (a<N){
		return false;
	}
	else 
	{
		return true;
	}
}
//-----------------------------------
//---------Error_Class---------------
//-----------------------------------
Error_Stop::Error_Stop(float a,float b) 
{
	this->Min=a;
	this->Error=b;
}
Error_Stop::~Error_Stop(){};

bool Error_Stop::Test(int a,Function* func,const VectorXf  & c,const VectorXf  & d,double t){
	float b;
	b=func->evaluate(c,d);
	if (abs(b-this->Min)>this->Error) {
		return false;
	}
	else 
	{
		return true;
	}
}
//Change_Class

//Time_Class
Time_Stop::Time_Stop(int cores,float t,float now) 
{
	this->Time=t;
	this->Start=now;
	this->c=cores;

}
Time_Stop::~Time_Stop(){};

bool Time_Stop::Test(int a,Function* func,const VectorXf  & c,const VectorXf  & d,double t){
	if (((t-this->Start)/(CLOCKS_PER_SEC*this->c))>this->Time) {
		return true;
	}
	else
	{
		return false;
	}
}