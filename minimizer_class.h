#pragma once
#include "function_class.h"
#include "loader_class.h"
#include "stopping_criteria_class.h"

using namespace Eigen;

class Minimizer {
   public: 
   	Function * func;
	Minimizer();
	~Minimizer();
	VectorXf Get_Min();
 };

  class Full_Gradient : public Minimizer {
    public: 
		Full_Gradient(Function*);
		~Full_Gradient();
		float Optimize(std::string, Stop_Criteria*, std::string);

		//VectorXf evaluate( const VectorXf &,const VectorXf &);
		//void PassData();
		//EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
 };

  class Random_Coordinate_Descend : public Minimizer {
    public: 
		Random_Coordinate_Descend(Function*);
		~Random_Coordinate_Descend();
		float Optimize(std::string, Stop_Criteria*, std::string);
		//VectorXf evaluate( const VectorXf &,const VectorXf &);
		//void PassData();
		//EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
 };

   class Parallel_Random_Coordinate_Descend : public Minimizer {
    public: 
		Parallel_Random_Coordinate_Descend(Function*);
		~Parallel_Random_Coordinate_Descend();
		float Optimize(int, std::string, Stop_Criteria*, std::string);

		//VectorXf evaluate( const VectorXf &,const VectorXf &);
		//void PassData();
		//EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
 };
