import random
import time
import h5py
import numpy as np
import math
from scipy import sparse

def Create_Least_Squares_Data(name,n,m,d):
	f=h5py.File(name, "w", libver='latest')
	data=f.create_dataset("data", (n,m), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	lab=f.create_dataset("labels", (n,1), dtype='f8', shuffle=True, chunks=True, compression="gzip", compression_opts=1)
	A=np.random.rand(n,1);
	lab[:,:]=A
	B=	sparse.rand(n, m, density=d, format='coo', dtype=None, random_state=None)
	B=B.todense()+np.matrix(np.identity(n))
	data[:,:]=B
	f.close()

Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_10_m_10_sparse.h5",10,10,0.5)
#Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_100_m_100_sparse.h5",100,100,0.5)
Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_1000_m_1000_sparse.h5",1000,1000,0.5)
#Create_Least_Squares_Data("../biglearning_data/LeSqr_Data_n_10000_m_10000_sparse.h5",10000,10000,0.5)
